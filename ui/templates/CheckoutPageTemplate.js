import {
  DeliveryAddress,
  PriceSummary,
  DeliverySchedule,
  ContactNumber,
} from "@/molecules";

import { uiContext } from "@/context/ManagedUiContext";

const CheckoutPageTemplate = ({ register, handleSubmit, onSubmit }) => {
  const { cart, text } = uiContext();

  return (
    <div className="py-10 2xl:py-12 checkout mx-auto max-w-[1920px] px-4 md:px-6 lg:px-8 2xl:px-10">
      <div className="flex flex-col mx-auto xl:max-w-screen-xl">
        <div className="flex flex-col flex-wrap grid-cols-1 gap-x-7 xl:gap-x-8 lg:grid lg:grid-cols-12">
          <div className="w-full col-start-1 col-end-9">
            <form onSubmit={handleSubmit(onSubmit)}>
              <div className="border rounded-md border-border-base text-brand-light">
                <DeliveryAddress {...{ register }} />
                <DeliverySchedule {...{ register }} />
                <ContactNumber {...{ register }} />
              </div>
              <input type="submit" />
            </form>
          </div>
          <PriceSummary {...{ cart }} />
        </div>
      </div>
    </div>
  );
};

export default CheckoutPageTemplate;
