import DefaultLayout from "@/components/layouts/DefaultLayout";
import CheckoutPageTemplate from "@/templates/CheckoutPageTemplate";
import { useForm } from "react-hook-form";

const Checkout = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const onSubmit = (data) => console.log(data);
  console.log(errors);

  return (
    <DefaultLayout {...{ pageTitle: "", pageMetaTitle: "", heading: "" }}>
      <div>
        <CheckoutPageTemplate {...{ register, handleSubmit, onSubmit }} />
      </div>
    </DefaultLayout>
  );
};

export default Checkout;
