const Modal = ({ isOpen, onCloseModal, children }) => {
  if (!isOpen) {
    return null;
  }

  return (
    <div className="modal-overlay">
      <div className="modal-content">
        <button className="modal-close" onClick={onCloseModal}>
          Close
        </button>
        {children}
      </div>
    </div>
  );
};

export default Modal;
