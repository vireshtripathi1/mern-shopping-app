import Image from "next/image";

import { Text } from "@/atoms";

const PriceSummary = ({ cart }) => (
  <div className="w-full col-start-9 col-end-13 mt-7 lg:mt-0">
    <div className="px-4 pt-4 border rounded-md border-border-base text-brand-light xl:py-6 xl:px-7">
      <div className="flex justify-between pb-2 text-sm font-semibold rounded-md text-heading">
        <span className="font-medium text-15px text-brand-dark">Product</span>
        <span className="font-medium ml-auto shrink-0 text-15px text-brand-dark">
          Subtotal
        </span>
      </div>
      {cart.map(({ Product, id }, index) => (
        <div className="flex items-center py-4 border-b border-border-base ">
          <div className="flex w-16 h-16 border rounded-md border-border-base shrink-0">
            <Image
              alt={Product.ProductMedia.name}
              src={`/product/${
                Product.ProductMedia[0]?.source
                  ? Product.ProductMedia[0]?.source
                  : "placeholder.svg"
              }`}
              width="64"
              height="64"
              className="object-cover bg-fill-thumbnail"
            />
          </div>
          <Text
            {...{
              content: Product.title,
              className: "font-normal text-15px text-brand-dark pl-3 pr-3",
            }}
          />

          <div className="flex font-normal ml-auto mr-auto text-15px text-brand-dark pl-2 pr-2 shrink-0">
            $7.80
          </div>
        </div>
      ))}
      <div className="flex items-center w-full py-4 text-sm font-medium border-b lg:py-5 border-border-base text-15px text-brand-dark last:border-b-0 last:text-base last:pb-0">
        Subtotal
        <span className="font-normal ml-auto mr-auto shrink-0 text-15px text-brand-dark">
          $17.80
        </span>
      </div>
      <div className="flex items-center w-full py-4 text-sm font-medium border-b lg:py-5 border-border-base text-15px text-brand-dark last:border-b-0 last:text-base last:pb-0">
        Shipping
        <span className="font-normal ml-auto mr-auto shrink-0 text-15px text-brand-dark">
          $0
        </span>
      </div>
      <div className="flex items-center w-full py-4 text-sm font-medium border-b lg:py-5 border-border-base text-15px text-brand-dark last:border-b-0 last:text-base last:pb-0">
        Total
        <span className="font-normal ml-auto mr-auto shrink-0 text-15px text-brand-dark">
          $17.80
        </span>
      </div>
      <button
        data-variant="formButton"
        className="group text-[13px] md:text-sm lg:text-15px leading-4 inline-flex items-center cursor-pointer transition ease-in-out duration-300 font-body font-semibold text-center justify-center tracking-[0.2px] rounded placeholder-white focus-visible:outline-none focus:outline-none h-11 md:h-[50px] bg-brand text-brand-light font-manrope px-5 lg:px-6 py-4 md:py-3.5 lg:py-4 hover:text-white hover:bg-opacity-90 focus:bg-opacity-70 w-full mt-8 mb-5 rounded font-semibold px-4 py-3 transition-all !bg-brand !text-brand-light"
      >
        Order Now
      </button>
    </div>
    <p className="text-brand-muted text-sm leading-7 lg:leading-[27px] lg:text-15px mt-8">
      By placing your order, you agree to be bound by the BoroBazar{" "}
      <a className="font-medium underline text-brand" href="/antique/terms">
        Terms of Service{" "}
      </a>
      and{" "}
      <a className="font-medium underline text-brand" href="/antique/privacy">
        Privacy
      </a>
      . Your credit/debit card data will not saved.
    </p>
    <p className="text-brand-muted text-sm leading-7 lg:leading-[27px] lg:text-15px mt-4">
      A bag fee may be added to your final total if required by law or the
      retailer. The fee will be visible on your receipt after delivery.
    </p>
  </div>
);

export default PriceSummary;
