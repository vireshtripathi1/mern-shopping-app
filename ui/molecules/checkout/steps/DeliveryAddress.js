import { Button, InputField, Modal, Text } from "@/atoms";

const addressObj = [
  {
    label: "Home",
    completeAddress:
      "Wolfson Institute of Preventive Medicine, London EC1M 7BA, UK",
    isDefault: true,
  },
  {
    label: "Office",
    completeAddress: "80 Windsor Park Rd, Singapore 574175",
    isDefault: false,
  },
];

const DeliveryAddress = ({ register }) => {
  return (
    <div className="accordion__panel border-b border-border-base expanded ">
      <div className="flex items-center p-4 pb-6 cursor-pointer sm:p-8 accordion__button">
        <Text
          {...{
            content: " 1",
            HtmlTag: "span",
            className:
              "flex items-center justify-center font-semibold border-2 border-current rounded-full h-9 w-9 text-brand  mr-3 ml-3",
          }}
        />
        <Text
          {...{
            content: " Delivery Address",
            className: "text-brand-dark text-15px sm:text-base font-semibold",
          }}
        />
      </div>
      <div className="pb-6  lg:pl-20 lg:pr-20 sm:pr-9 sm:pl-9 pr-5 pl-5 accordion__content">
        <div className="mb-6">
          <div className="flex flex-col justify-between h-full -mt-4 text-15px md:mt-0">
            <div className="space-y-4 md:grid md:grid-cols-2 md:gap-5 auto-rows-auto md:space-y-0">
              {addressObj.map(
                ({ label, completeAddress, isDefault }, index) => (
                  <label
                    key={index}
                    className="hover:border-brand checked:border-brand  border-2 relative focus:outline-none rounded-md p-5 block cursor-pointer min-h-[112px] h-full group address__box"
                  >
                    <InputField
                      {...{
                        register: {
                          ...register("deliveryAddress", { required: true }),
                        },
                        type: "radio",
                        dbName: "deliveryAddress",
                        name: "deliveryAddress",
                        value: completeAddress,
                      }}
                    />
                    <Text
                      {...{
                        content: label,
                        className: "mb-2 -mt-1 font-semibold text-brand-dark",
                      }}
                    />
                    <Text
                      {...{
                        content: completeAddress,
                        className: "leading-6 text-brand-muted",
                      }}
                    />

                    <div className="absolute z-10 flex left-64 top-3 address__actions">
                      <button className="flex items-center justify-center w-6 h-6 text-base rounded-full bg-brand text-brand-light text-opacity-80">
                        Edit
                      </button>
                    </div>
                  </label>
                )
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DeliveryAddress;
