import { InputField, Text } from "@/atoms";

const DeliverySchedule = ({ register }) => (
  <div className="accordion__panel border-b border-border-base  ">
    <div className="flex items-center p-4 pb-6 cursor-pointer sm:p-8 accordion__button">
      <Text
        {...{
          content: " 2",
          HtmlTag: "span",
          className:
            "flex items-center justify-center font-semibold border-2 border-current rounded-full h-9 w-9 text-brand  mr-3 ml-3",
        }}
      />
      <Text
        {...{
          content: " Delivery Schedule",
          className: "text-brand-dark text-15px sm:text-base font-semibold",
        }}
      />
    </div>
    <div className="pb-6  lg:pl-20 lg:pr-20 sm:pr-9 sm:pl-9 pr-5 pl-5 accordion__content">
      <div className="mb-6">
        <div className="w-full">
          <div className="w-full mx-auto">
            <div>
              <div
                className="grid grid-cols-2 gap-4 sm:grid-cols-4 lg:grid-cols-6"
                role="none"
              >
                <div className="relative rounded-lg px-5 py-3 cursor-pointer focus:outline-none bg-brand text-brand-light">
                  <div className="text-center">
                    <p className="text-base font-semibold  text-brand-light">
                      Sat
                    </p>
                    <span className="text-15px text-brand-light">Jul 03</span>
                  </div>
                </div>
              </div>
            </div>
            <div className="mt-10">
              <label className="sr-only">Delivery Schedule</label>
              <div className="flex flex-wrap justify-between grid-cols-2 gap-4 lg:grid sm:grid-cols-3 lg:grid-cols-5">
                <div className="cursor-pointer focus:outline-none">
                  <label className="flex items-center">
                    <InputField
                      {...{
                        register: {
                          ...register("deliverySchedule", { required: true }),
                        },
                        type: "radio",
                        dbName: "deliverySchedule",
                        name: "deliverySchedule",
                        value: "9am to 10am",
                      }}
                    />
                    9am to 10am
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default DeliverySchedule;
