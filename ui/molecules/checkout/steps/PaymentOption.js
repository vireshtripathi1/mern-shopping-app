const PaymentOption = () => (
  <div
    className="accordion__panel border-b border-border-base 
                   "
  >
    <div
      id="index_3"
      className="flex items-center p-4 pb-6 cursor-pointer sm:p-8 accordion__button"
    >
      <span className="flex items-center justify-center font-semibold border-2 border-current rounded-full h-9 w-9 text-brand mr-3 ml-3">
        4
      </span>
      <h3 className="text-brand-dark text-15px sm:text-base font-semibold">
        Payment Option
      </h3>
    </div>
    <div
      data-aria-label="index_3"
      className="pb-6 pl-5 pr-5 sm:pl-9 sm:pr-9 lg:pl-20 lg:pr-20 sm:pr-9 sm:pl-9 pr-5 pl-5 accordion__content"
    >
      <div className="mb-6">
        <div className="w-full bg-white rounded-xl xl:w-[500px]">
          <h3 className="text-brand-dark opacity-60 mb-3">Enter card info</h3>
          <div className="StripeElement StripeElement--empty">
            <div className="__PrivateStripeElement">
              <iframe
                name="__privateStripeFrame83712"
                frameborder="0"
                allowtransparency="true"
                scrolling="no"
                role="presentation"
                allow="payment *"
                src="https://js.stripe.com/v3/elements-inner-card-544f854ea5c303d66684e5fe097cf46a.html#wait=false&amp;mids[guid]=47788e56-d0d8-4442-85ee-b39d45a6e94d7aab64&amp;mids[muid]=d0a53eb8-e79a-4a08-92e3-883fb49e0909cd9a83&amp;mids[sid]=d580250e-d33d-4fbe-9b7b-273d0d1fbbbcd871e0&amp;rtl=false&amp;componentName=card&amp;keyMode=unknown&amp;apiKey=Put+your+stripe+public+key+here&amp;referrer=https%3A%2F%2Fborobazar.vercel.app%2Fantique%2Fcheckout&amp;controllerId=__privateStripeController8371"
                title="Secure card payment input frame"
              ></iframe>
              <input
                className="__PrivateStripeElement-input"
                aria-hidden="true"
                aria-label=" "
                autocomplete="false"
                maxlength="1"
              />
            </div>
          </div>
          <button
            data-variant="formButton"
            className="group text-[13px] md:text-sm lg:text-15px leading-4 inline-flex items-center cursor-pointer transition ease-in-out duration-300 font-body font-semibold text-center justify-center tracking-[0.2px] rounded placeholder-white focus-visible:outline-none focus:outline-none h-11 md:h-[50px] bg-brand text-brand-light font-manrope px-5 lg:px-6 py-4 md:py-3.5 lg:py-4 hover:text-white hover:bg-opacity-90 focus:bg-opacity-70 h-11 md:h-12  mt-5"
            type="button"
          >
            Pay Now
          </button>
        </div>
      </div>
      <div className="text-right text-left">
        <button
          data-variant="formButton"
          className="group text-[13px] md:text-sm lg:text-15px leading-4 inline-flex items-center cursor-pointer transition ease-in-out duration-300 font-body font-semibold text-center justify-center tracking-[0.2px] rounded placeholder-white focus-visible:outline-none focus:outline-none h-11 md:h-[50px] bg-brand text-brand-light font-manrope px-5 lg:px-6 py-4 md:py-3.5 lg:py-4 hover:text-white hover:bg-opacity-90 focus:bg-opacity-70 px-4 py-3 text-sm font-semibold rounded bg-brand text-brand-light"
        >
          Next Steps
        </button>
      </div>
    </div>
  </div>
);

export default PaymentOption;
